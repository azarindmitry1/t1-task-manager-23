package ru.t1.azarin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "task-start-by-id";

    @NotNull
    public final static String DESCRIPTION = "Start task by id.";

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        serviceLocator.getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
