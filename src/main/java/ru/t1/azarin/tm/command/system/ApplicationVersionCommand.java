package ru.t1.azarin.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    public final static String NAME = "version";

    @NotNull
    public final static String ARGUMENT = "-v";

    @NotNull
    public final static String DESCRIPTION = "Display application version.";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.23.0");
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
