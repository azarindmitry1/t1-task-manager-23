package ru.t1.azarin.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface ICommand {

    void execute();

    @Nullable
    String getName();

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

}
